package ;
import kha.math.Vector2;
import kha.Canvas;
import kha.Loader;
import SpriteLampGraphic;
import com.khapunk.graphics.Animator.CallbackFunction;
import com.khapunk.graphics.Spritemap;

/**
 * ...
 * @author Sidar Talei
 */
class SpriteLampSpriteMap extends Spritemap
{

	public var material:SpriteLampMaterial;
	public var z(default, default) : Float = 0;
	
	public function new(diffuse:String, frameWidth:Int = 0, frameHeight:Int = 0, cb:CallbackFunction = null, name:String = "") 
	{
		var diffusefile:String = (diffuse + "_diffuse");
		
		material = new SpriteLampMaterial();
		
		super(diffusefile, frameWidth, frameHeight, cb, name);
		
		material.celLevel = 0;
		
		//material.aoMap = Loader.the.getImage(diffuse + "_ao");
		//material.emissiveMap = Loader.the.getImage(diffuse + "_emessive");
		material.normalMap = Loader.the.getImage(diffuse + "_normal");
		//material.specGlossMap = Loader.the.getImage(diffuse + "_spec");	
	}
	
	override public function render(buffer:Canvas, point:Vector2, camera:Vector2) 
	{
		
		SpriteLampScene.spriteLamp.setNormalMap(material.normalMap);
		SpriteLampScene.spriteLamp.setAOMap(material.aoMap);
		SpriteLampScene.spriteLamp.setSpecMap(material.specGlossMap);
		SpriteLampScene.spriteLamp.setResolution(_region.w , _region.h);
		SpriteLampScene.spriteLamp.setShininess(material.shininess);
		SpriteLampScene.spriteLamp.setCelLevel(material.celLevel);
		SpriteLampScene.spriteLamp.setEmissiveMap(material.emissiveMap);

		
		SpriteLampGraphic.pos3D.x = point.x + x - originX * (scale * scaleX) - camera.x * scrollX + (_region.w*(scale * scaleX))/2;
		SpriteLampGraphic.pos3D.y = point.y + y - originY * (scale * scaleY) - camera.y * scrollY + (_region.h*(scale * scaleY))/2;
		SpriteLampGraphic.pos3D.z = z;
		
		SpriteLampScene.spriteLamp.setPosition(SpriteLampGraphic.pos3D);
		
		super.render(buffer, point, camera);
	}
	 
}