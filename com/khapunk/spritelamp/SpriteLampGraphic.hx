package ;
import kha.math.Vector2;
import kha.math.Vector3;

import com.khapunk.graphics.PunkImage;
import kha.Canvas;
import kha.Image;
import kha.Rectangle;

/**
 * ...
 * @author Sidar Talei
 */
class SpriteLampGraphic extends PunkImage
{

	public var material:SpriteLampMaterial;
	public static var pos3D(get, null):Vector3;
	public static function get_pos3D() : Vector3 {
		if (pos3D == null) pos3D = new Vector3();
		return pos3D;
	};
	
	public var z(default, default) : Float = 0;
	
	public function new(source:Dynamic, normal:Image, spec:Image, ao:Image, emissive:Image ) 
	{
		pos3D = new Vector3();
		material = new SpriteLampMaterial();
		material.normalMap = normal;
		material.specGlossMap = spec;
		material.aoMap = ao;
		material.emissiveMap = emissive;
		material.celLevel = 2;
		super(source, null, "");
	}
	
	override public function render(buffer:Canvas, point:Vector2, camera:Vector2) 
	{
	
		SpriteLampScene.spriteLamp.setNormalMap(material.normalMap);
		SpriteLampScene.spriteLamp.setAOMap(material.aoMap);
		SpriteLampScene.spriteLamp.setSpecMap(material.specGlossMap);
		SpriteLampScene.spriteLamp.setResolution(_region.w , _region.h);
		SpriteLampScene.spriteLamp.setShininess(material.shininess);
		SpriteLampScene.spriteLamp.setCelLevel(material.celLevel);
		SpriteLampScene.spriteLamp.setEmissiveMap(material.emissiveMap);

		
		pos3D.x = point.x + x - originX * (scale * scaleX) - camera.x * scrollX + (_region.w*(scale * scaleX))/2;
		pos3D.y = point.y + y - originY * (scale * scaleY) - camera.y * scrollY + (_region.h*(scale * scaleY))/2;
		pos3D.z = z;
		
		SpriteLampScene.spriteLamp.setPosition(pos3D);
		
		super.render(buffer, point, camera);
	}
}

class SpriteLampMaterial {
	public var normalMap:Image;
	public var specGlossMap:Image;
	public var aoMap:Image;
	public var emissiveMap:Image;
	
	public var shininess:Float = 1;
	public var celLevel:Float = 1;
	
	public function new(){}
	
}