package ;

import com.khapunk.Entity;
import com.khapunk.Graphic;
import com.khapunk.KP;
import com.khapunk.Mask;
import com.khapunk.utils.Input;
import com.khapunk.utils.PunkKey;
import kha.Canvas;
import kha.math.Vector3;

/**
 * ...
 * @author Sidar Talei
 */
class SpriteLampEntity extends Entity
{

	var upperColor:Vector3;
	var lowerColor:Vector3;
	
	
		
	
	var m:Vector3;
	public function new(x:Float=0, y:Float=0, mask:Mask=null) 
	{
		
		upperColor = new Vector3(.6, .6, .6);
		lowerColor = new Vector3(.5,.5, .5);
		
		super(x, y, graphic, mask);
		m = new Vector3();
		Input.define("ChangeColor", [ PunkKey.G ]);
	}
	
	override public function render(buffer:Canvas):Void 
	{
		if (graphic != null && graphic.visible)
		{	
			if (graphic.relative)
			{
				point.x = x;
				point.y = y;
			}
			else point.x = point.y = 0;
			camera.x = _scene == null ? KP.camera.x : _scene.camera.x;
			camera.y = _scene == null ? KP.camera.y : _scene.camera.y;

		
			//if (Empty.spriteLamp != null || Empty.spriteLamp != buffer.g2.program) {	
			buffer.g2.program = SpriteLampScene.spriteLamp;
				
			//}
			m.x = KP.scene.mouseX;
			m.y = KP.scene.mouseY;
			m.z = -100;
			 SpriteLampScene.spriteLamp.setLightPos(0, m);
			graphic.render(buffer, point, camera);
			
			/*if (Input.mouseDown) {
			trace("jaaaaa");
				
			var r:Float = Math.random() ;
			var g:Float = Math.random() ;
			var b:Float = Math.random() ;
			
			upperColor.x = r;
			upperColor.y = g;
			upperColor.z = b;
			
			r = Math.random() *  Math.random();
			g = Math.random() *  Math.random();
			b = Math.random() *  Math.random();
			
			lowerColor.x = r;
			lowerColor.y = g;
			lowerColor.z = b;
			
			
			
			SpriteLampScene.spriteLamp.setUpperAmbientColour(upperColor);
			SpriteLampScene.spriteLamp.setLowerAmbientColour(lowerColor);
			
		}*/
			
		}
	}
	

	
}

