package ;

import com.khapunk.Engine;
import com.khapunk.KP;
import com.khapunk.Scene;
import com.khapunk.utils.Input;
import com.khapunk.utils.PunkKey;
import kha.graphics4.BlendingOperation;
import kha.Image;
import kha.math.Vector3;
import Shaders.programs.SpriteLamp;

/**
 * ...
 * @author Sidar Talei
 */
class SpriteLampScene extends Scene
{

	public static var spriteLamp:SpriteLamp;
	var upperColor:Vector3;
	var lowerColor:Vector3;
	
	public function new() 
	{
		super();
		
	}
	
	override public function begin() 
	{
		upperColor = new Vector3(0, 1, 1);
		lowerColor = new Vector3(1,0, 0);
		
		if (spriteLamp == null) {
			spriteLamp = new SpriteLamp();
			
			Engine.backbuffer.g2.program = spriteLamp;
			spriteLamp.setGraphic(Engine.backbuffer.g4);
			spriteLamp.setLightCount(1);
			spriteLamp.setLightColor(0, new Vector3(1, 1, 1));
			spriteLamp.setUpperAmbientColour(upperColor);
			spriteLamp.setLowerAmbientColour(lowerColor);
			spriteLamp.setWrapAroundLevel(0);
		
		}
		
		
	}
	
	override public function render(buffer:Image):Void 
	{
		super.render(buffer);
		buffer.g2.program = null;
	}
	
	override public function update():Void 
	{
		super.update();
	}
	
}